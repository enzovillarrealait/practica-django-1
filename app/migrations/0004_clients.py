# Generated by Django 3.2.5 on 2021-07-12 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0003_rename_books_book"),
    ]

    operations = [
        migrations.CreateModel(
            name="Clients",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("age", models.IntegerField()),
                ("address", models.CharField(max_length=255)),
                ("nationality", models.CharField(max_length=128)),
                ("national_id", models.IntegerField()),
                ("is_active", models.BooleanField(default=True)),
                ("phone_number", models.IntegerField()),
                ("email", models.EmailField(max_length=254)),
            ],
        ),
    ]
