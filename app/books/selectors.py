import operator

from django.db.models import Q

from app.models import Book

from django.db.models.functions import Concat

import functools


def list_books():
    return Book.objects.all()


# PARA CASOS SIMPLES POR DESCRIPCION (matchear por varios campos a la vez, sin importar el orden de las palabras)
# def filter_books(*, description: str = "", **kwargs):
#     # 1-  lista de comprencion, split de descipcion
#     # 2- Se arma la query con Q() por cada palabra en la descripcion, asi se obtiene un lista de filtros por cada palabra
#     # 3- En este caso con reduce() se genera uin cadena de AND de  querys
#
#     query = functools.reduce(
#         operator.and_,
#         [Q(concat_field__icontains=d) for d in description.split(" ")],
#     )
#
#     return Book.objects.annotate(
#         concat_field=Concat("author__name", "title")
#     ).filter(query)


# Casos simples (un campo a la vez, el orden las palabras si importa)
def filter_books(*, description: str = "", **kwargs):
    return Book.objects.filter(
        Q(author__name__contains=description)
        | Q(title__startswith=description)
    )
