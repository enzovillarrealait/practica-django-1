from django.urls import path, include
from rest_framework.routers import DefaultRouter

# from .views import BooksViewSet

# router = DefaultRouter()
# router.register("books", BooksViewSet)
#
# urlpatterns = [path("", include(router.urls))]
from app.books.views import BookListApi, BookCreateApi, BookUpdateApi

urlpatterns = [
    path("list/", BookListApi.as_view()),
    path("create/", BookCreateApi.as_view()),
    path("update/<int:book_id>/", BookUpdateApi.as_view()),
]
