from app.models import Book, Author

from rest_framework.serializers import ModelSerializer


class AuthorSerializer(ModelSerializer):
    class Meta:
        # REFERENCIA AL MODELO
        model = Author
        # MOSTRAR TODOS LOS CAMPOS
        fields = "__all__"


class BookSerializer(ModelSerializer):

    author = AuthorSerializer()

    class Meta:
        # REFERENCIA AL MODELO
        model = Book
        # MOSTRAR TODOS LOS CAMPOS
        fields = "__all__"
