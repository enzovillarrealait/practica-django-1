from app.models import Book


def book_create(*, title: str, description: str):
    return Book.objects.create(title=title, description=description)


def book_update(*, id: int, title: str, description: str, is_active: bool):

    # A
    try:
        book = Book.objects.get(pk=id)
        book.title = title
        book.description = description
        book.is_active = is_active
        book.save()
        return book
    except Book.DoesNotExist:
        return None

    # OPCION B
    # no retorna el elemento actualizado
    # return Book.objects.filter(id=id).update(
    #     description=description, title=title, is_active=is_active
    # )
