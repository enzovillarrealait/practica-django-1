from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from app.books.selectors import list_books, filter_books
from app.books.services import book_create, book_update
from app.helpers import CustomPageNumberPagination

from app.books.serializers import BookSerializer

from rest_framework.viewsets import ModelViewSet


class BookListApi(APIView, CustomPageNumberPagination):
    def get(self, request):

        query_params = request.query_params
        diccionario = {i: query_params.get(i) for i in query_params}

        books = filter_books(**diccionario)

        paginated_books = self.paginate_queryset(books, request)

        return self.get_paginated_response(
            BookSerializer(paginated_books, many=True).data
        )


class BookCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        title = serializers.CharField(max_length=128)
        description = serializers.CharField(max_length=255)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        book = book_create(**serializer.validated_data)

        return Response(BookSerializer(book).data, status=201)


class BookUpdateApi(APIView):
    class InputSerializer(serializers.Serializer):
        title = serializers.CharField(max_length=128)
        description = serializers.CharField(max_length=255)
        is_active = serializers.BooleanField()

    def put(self, request, book_id):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        book = book_update(**serializer.validated_data, id=book_id)

        if book:
            return Response(BookSerializer(book).data, status=201)
        else:
            return Response({"error": "Id no existe"}, status=201)
