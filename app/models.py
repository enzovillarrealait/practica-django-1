from django.db import models

# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    author = models.ForeignKey(
        "app.Author", on_delete=models.DO_NOTHING, null=True
    )


class Author(models.Model):
    name = models.CharField(max_length=128)


class Nationalities(models.Model):
    description = models.CharField(max_length=128, null=True, default=None)


class Clients(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    address = models.CharField(max_length=255)
    nationality_old = models.CharField(max_length=128, default="")
    nationality = models.ForeignKey(
        "app.Nationalities",
        default=None,
        null=True,
        on_delete=models.DO_NOTHING,
    )
    national_id = models.IntegerField()
    is_active = models.BooleanField(default=True)
    phone_number = models.IntegerField()
    email = models.EmailField(max_length=254)
