from abc import ABC

from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from app.clients.selectors import (
    get_client,
    filter_clients,
    list_nationalities,
)
from app.clients.serializers import ClientSerializer, NationalitySerializer
from app.clients.services import client_create, client_update
from app.models import Nationalities


class CustomPageNumberPagination(PageNumberPagination):
    page_size = 15


class ClientsListApi(APIView, CustomPageNumberPagination):
    def get(self, request):
        clients = None
        query_params = request.query_params
        diccionario = {i: query_params.get(i) for i in query_params}

        diccionario = {
            key.replace("[]", ""): query_params.get(key)
            if not "[]" in key
            else query_params.getlist(key)
            for key in query_params
        }

        clients = filter_clients(**diccionario)

        paginated_clients = self.paginate_queryset(clients, request, view=self)

        return self.get_paginated_response(
            ClientSerializer(paginated_clients, many=True).data
        )


class ClientGetApi(APIView):
    def get(self, request, client_id):
        client = get_client(id=client_id)
        if client:
            return Response(ClientSerializer(client).data, status=201)
        else:
            return Response({"error": "ID no existe"}, status=400)


class ClientCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=255)
        age = serializers.IntegerField()
        address = serializers.CharField(max_length=255)
        nationality = serializers.IntegerField()
        national_id = serializers.IntegerField()
        phone_number = serializers.IntegerField()
        email = serializers.EmailField(max_length=254)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        client = client_create(**serializer.validated_data)

        return Response(ClientSerializer(client).data, status=201)


class ClientUpdateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=255)
        age = serializers.IntegerField()
        address = serializers.CharField(max_length=255)
        nationality = serializers.IntegerField()
        national_id = serializers.IntegerField()
        phone_number = serializers.IntegerField()
        email = serializers.EmailField(max_length=254)
        is_active = serializers.BooleanField()

    def put(self, request, client_id):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        client = client_update(**serializer.validated_data, id=client_id)

        if client:
            return Response(ClientSerializer(client).data, status=201)
        else:
            return Response({"error": "ID no existe"}, status=400)


class ClientsUpdateNationalityApi(APIView):
    def get(self, request):
        clients = filter_clients()

        for client in clients:
            nationality, created = Nationalities.objects.get_or_create(
                description=client.nationality
            )
            client.nationality_link = nationality
            client.save()

        clients = filter_clients()

        return Response(ClientSerializer(clients, many=True).data, status=201)


class NationalitiesListApi(APIView):
    def get(self, request):
        nationalities = list_nationalities()
        return Response(
            NationalitySerializer(nationalities, many=True).data, status=200
        )
