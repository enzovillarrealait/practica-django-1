from django.db.models import Q

from app.models import Clients, Nationalities


def list_clients():
    return Clients.objects.all()


def list_nationalities():
    return Nationalities.objects.all()


def get_client(*, id: int):
    try:
        return Clients.objects.get(pk=id)
    except Clients.DoesNotExist:
        return None


def filter_clients_decription(clients, description):
    return clients.filter(
        Q(name__icontains=description)
        | Q(nationality__description__icontains=description)
        | Q(email__startswith=description)
        | Q(address__icontains=description)
        | Q(national_id__startswith=description)
    )


def filter_clients_nationality(clients, nationality):
    return clients.filter(Q(nationality__in=nationality))


def filter_clients(*, description: str = "", nationality=[], **kwargs):
    # Filter by name, address, email, nationality, national_id

    clients = list_clients()
    if description:
        clients = filter_clients_decription(clients, description)
    if nationality:
        clients = filter_clients_nationality(clients, nationality)

    return clients
