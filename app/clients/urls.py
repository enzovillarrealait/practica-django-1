from django.urls import path, include

from app.clients.views import (
    ClientsListApi,
    ClientCreateApi,
    ClientUpdateApi,
    ClientGetApi,
    ClientsUpdateNationalityApi,
    NationalitiesListApi,
)

urlpatterns = [
    path("list/", ClientsListApi.as_view()),
    path("list/<client_id>/", ClientGetApi.as_view()),
    path("create/", ClientCreateApi.as_view()),
    path("update/<client_id>/", ClientUpdateApi.as_view()),
    path("updated-nation/", ClientsUpdateNationalityApi.as_view()),
    path("nationalities/list/", NationalitiesListApi.as_view()),
]
