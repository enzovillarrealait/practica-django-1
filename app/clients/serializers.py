from rest_framework.serializers import ModelSerializer

from app.models import Clients, Nationalities


class NationalitySerializer(ModelSerializer):
    class Meta:
        model = Nationalities
        fields = "__all__"


class ClientSerializer(ModelSerializer):

    nationality = NationalitySerializer()

    class Meta:
        model = Clients
        fields = "__all__"
