from app.models import Clients


def client_create(
    *,
    name: str,
    age: int,
    address: str,
    nationality: str,
    national_id: int,
    phone_number: int,
    email: str
):
    return Clients.objects.create(
        name=name,
        age=age,
        address=address,
        nationality=nationality,
        national_id=national_id,
        phone_number=phone_number,
        email=email,
    )


def client_update(
    *,
    id: int,
    name: str,
    age: int,
    address: str,
    nationality: str,
    national_id: int,
    is_active: bool,
    phone_number: int,
    email: str
):
    try:
        client = Clients.objects.get(pk=id)
        client.name = name
        client.age = age
        client.address = address
        client.nationality = nationality
        client.nacional_id = national_id
        client.is_active = is_active
        client.phone_number = phone_number
        client.email = email
        client.save()
        return client

    except Clients.DoesNotExist:
        return None
